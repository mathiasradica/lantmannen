<?php

$upper_dir = "~/Vaimo/agri_prod_xml/archive/";

foreach (scandir($upper_dir) as $lower_dir){
    
    if ($lower_dir != ".DS_Store"){

        foreach (scandir($upper_dir . $lower_dir) as $file_name) {

            if (pathinfo($file_name, PATHINFO_EXTENSION) == 'gz') {
                $buffer_size = 4096;
                $out_file_name = str_replace('.gz', '', $file_name);
                $file = gzopen($upper_dir . $lower_dir . "/" . $file_name, 'rb');
                $out_file = fopen("/Users/mathiasradica/Vaimo/agri_prod_xml/archive_unzipped/" . $out_file_name, 'wb');
        
                while (!gzeof($file)) {
                    fwrite($out_file, gzread($file, $buffer_size));
                }
        
                fclose($out_file);
                gzclose($file);
            }
        }
    }
}