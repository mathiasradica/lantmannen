<?php

$dir_processed = "~/Vaimo/agri_prod_xml/processed/";
$dir_archive = "~/Vaimo/agri_prod_xml/archive_unzipped/";

$servername = "127.0.0.1";
$username = "root";
$password = "";
$dbname = "lantmannen_full";

$products = [];
$conn = new mysqli($servername, $username, $password, $dbname);
$result_file = fopen(__DIR__ . "/test_result.txt", "w");

$start_1 = microtime(true);
$products = xml_to_array($dir_arcive);
$skus = array_keys($products);
array_walk($skus, function (&$sku) {$sku = '\'' . $sku . '\'';});
$sku_string = '(' . implode(', ', $skus) . ')';
echo microtime(true) - $start_1 . "\n";

$start_2 = microtime(true);
$attribute_result = base_query();
echo microtime(true) - $start_2 . "\n";

$start_3 = microtime(true);
$category_result = category_query();
echo microtime(true) - $start_3 . "\n";

$start_4 = microtime(true);
$image_result = data_query("C4ImageData");
echo microtime(true) - $start_4 . "\n";

$start_5 = microtime(true);
$doc_result = data_query("C4DocsData");
echo microtime(true) - $start_5 . "\n";

$start_6 = microtime(true);
$link_result = link_query();
echo microtime(true) - $start_6 . "\n";

$start_7 = microtime(true);
$r0 = $r1 = $r2 = $r3 = $r4 = $r5 = $r6 = $r7 = $r8 = $r9 = $r10 = $r11 = $r12 = $r13 = $r14 = $r15 = $r16 = $r17 = $r18 = $r19 = 0;
foreach ($products as $sku => $product) {
    if (!key_exists($sku, $attribute_result)) {
        fwrite($result_file, "SKU: " . $sku . "\nThe sku does not exist in the database.\n");
        $r0++;
        continue;
    } else if (has_string_keys($product['attributes'])) {
        foreach ($product['attributes'] as $key => $value) {
            if (!is_array($value)) {
                foreach ($attribute_result[$sku] as $attributes_group) {
                    if ($attributes_group['store'] == $product['attributes']['@attributes']['store']) {
                        if (key_exists($key, $attributes_group)) {
                            if ($attributes_group[$key] != $value &&
                                !($key == 'visibility' && ($attributes_group[$key] == 'both' || $attributes_group[$key] == '1') && ($value == 'both' || $value == '1')) &&
                                !($key == 'status' && ($attributes_group[$key] == 'enabled' || $attributes_group[$key] == '4') && ($value == 'enabled' || $value == '4'))) {
                                fwrite($result_file, "SKU: $sku \n The attribute $key in store " . $attributes_group['store'] . ": $value vs " . print_r($attributes_group[$key], true) . ".\n");
                                $r1++;
                            }
                        } else {
                            fwrite($result_file, "SKU: $sku \nThe attribute $key in store " . $attributes_group['store'] . " in the xml does not exist in the database.\n");
                            $r2++;
                        }
                        break;
                    }
                }
            }
        }
    } else {
        foreach ($product['attributes'] as $attributes_group_1) {
            foreach ($attributes_group_1 as $key => $value) {
                if (!is_array($value)) {
                    foreach ($attribute_result[$sku] as $attributes_group_2) {
                        if ($attributes_group_2['store'] == $attributes_group_1['@attributes']['store']) {
                            if (key_exists($key, $attributes_group_2)) {
                                if ($attributes_group_2[$key] != $value &&
                                    !($key == 'visibility' && ($attributes_group[$key] == 'both' || $attributes_group[$key] == '1') && ($value == 'both' || $value == '1')) &&
                                    !($key == 'status' && ($attributes_group[$key] == 'enabled' || $attributes_group[$key] == '4') && ($value == 'enabled' || $value == '4'))) {
                                    fwrite($result_file, "SKU: $sku \n The attribute $key in store " . $attributes_group_2['store'] . ": $value vs " . print_r($attributes_group_2[$key], true) . ".\n");
                                    $r1++;
                                }
                            } else {
                                fwrite($result_file, "SKU: $sku \nThe attribute $key in store " . $attributes_group_2['store'] . " in the xml does not exist in the database.\n");
                                $r2++;
                            }
                            break;
                        }
                    }
                }
            }
        }
    }

    $normalized_xml_strings = [];
    if (isset($product['categories']['category'])) {
        if (is_array($product['categories']['category'])) {
            if (isset($product['categories']['category']['@attributes'])) {
                foreach ($product['categories']['category']['@attributes'] as $category_group => $category) {
                    $normalized_xml_strings[] = strtolower(str_replace('"', '', str_replace('/-', '/', (str_replace('-/', '/', str_replace('--', '-', (
                        str_replace(' ', '-', str_replace(',', ' ', iconv('UTF-8', 'ASCII//TRANSLIT', str_replace('//', '/', $category)))))))))));
                }
            } else if (!has_string_keys($product['categories']['category'])) {
                $normalized_xml_string = '';
                foreach ($product['categories']['category'] as $category_group) {
                    $normalized_xml_strings[] = strtolower(str_replace('"', '', str_replace('/-', '/', (str_replace('-/', '/', str_replace('--', '-', (
                        str_replace(' ', '-', str_replace(',', ' ', iconv('UTF-8', 'ASCII//TRANSLIT', str_replace('//', '/', $category_group))))))))))) . '-' . $normalized_xml_string;
                }
                $normalized_xml_strings[] = $normalized_xml_string;
            }
        } else {
            $normalized_xml_strings[] = strtolower(str_replace('"', '', str_replace('/-', '/', (str_replace('-/', '/', str_replace('--', '-', (
                str_replace(' ', '-', str_replace(',', ' ', iconv('UTF-8', 'ASCII//TRANSLIT', str_replace('//', '/', $product['categories']['category'])))))))))));
        }
        $match = false;
        $categories_in_database = true;
        if (key_exists($sku, $category_result)) {
            if (count($normalized_xml_strings) == 1) {
                $xml_category_array = explode('/', $normalized_xml_strings[0]);
                sort($xml_category_array);

                $database_category_array = [];
                foreach (normalize_array($category_result[$sku]) as $category) {
                    $normalized_database_string = preg_replace('/^products\-\d{4,10}?\/|^produkter\-\d{4,10}?\/|^tuotteet\-\d{4,10}?\/|\-\d{4,10}/', '', $category);
                    $database_category_array = explode('/', $normalized_database_string);
                    sort($database_category_array);
                    if ($xml_category_array == $database_category_array) {
                        $match = true;
                        break;
                    }
                }
            } else if (count($normalized_xml_strings) > 1) {
                $match = true;
                foreach ($normalized_xml_strings as $normalized_xml_string) {
                    $xml_category_array = explode('/', $normalized_xml_string);
                    sort($xml_category_array);
                    $match_one = false;
                    foreach (normalize_array($category_result[$sku]) as $category) {
                        $normalized_database_string = str_replace('produkter/', '', str_replace('products/', '', str_replace('tuotteet/', '', preg_replace('/\-\d{4,10}/', '', $category))));
                        $database_category_array = explode('/', $normalized_database_string);
                        sort($database_category_array);
                        if ($xml_category_array == $database_category_array) {
                            $match_one = true;
                            break;
                        }
                    }
                    if (!$match_one) {
                        $match = false;
                        break;
                    }
                }
            }
        } else {
            $categories_in_database = false;
        }
        if (!$match) {
            if (!$categories_in_database) {
                fwrite($result_file, "SKU: " . $sku . "\nThere are categories specified in the xml but not in the database.\n");
                $r3++;
            } else {
                fwrite($result_file, "SKU: " . $sku . "\nThe categories in the database do not match those in the xml.\n");
                $r4++;
            }
        }
    } else {
        if (!key_exists($sku, $category_result)) {
            fwrite($result_file, "SKU: " . $sku . "\nThere are no categories specified in the database or in the xml.\n");
            $r5++;
        } else {
            fwrite($result_file, "SKU: " . $sku . "\nThere are categories specified in the database but not in the xml.\n");
            $r6++;
        }
    }

    if (key_exists($sku, $image_result)) {
        data_comparison($image_result[$sku], 'image', $sku, $product);
    }
    if (key_exists($sku, $doc_result)) {
        data_comparison($doc_result[$sku], 'doc', $sku, $product);
    }

    if (key_exists($sku, $link_result)) {
        if ($link_result[$sku] != $link = isset($product['links']['link']) ? normalize_array($product['links']['link']) : []) {
            fwrite($result_file, "SKU: " . $sku . "\n" . "The link data is:\n" . print_r($link, true) . "\nvs\n" . print_r($link_result[$sku], true) . "\n");
            $r19++;
        }
    }
}
echo microtime(true) - $start_7 . "\n\n";

echo "0) A sku from the xml does not exist in the database: $r0\n
1) An attribute differs between xml and database: $r1\n
2) The attribute in the xml does not exist in the database: $r2\n
3) There are categories specified in the xml but not in the database: $r3\n
4) The categories in the database do not match those in the xml: $r4\n
5) There are no categories specified in the database or in the xml: $r5\n
6) There are categories specified in the database but not in the xml: $r6\n
7) There is image data in the xml but not in the database: $r7\n
8) There is image data in the database but not in the xml: $r8\n
9) Image data: An attribute differs between xml and database: $r9\n
10) Image data: An attribute is not set in the database: $r10\n
11) Image data: An attribute is not set in the xml: $r11\n
12) The image data differs between xml and database: $r12\n
13) There is doc data in the xml but not in the database: $r13\n
14) There is doc data in the database but not in the xml: $r14\n
15) Doc data: An attribute differs between xml and database: $r15\n
16) Doc data: An attribute is not set in the database: $r16\n
17) Doc data: An attribute is not set in the xml: $r17\n
18) The doc data differs between xml and database: $r18\n
19) The link data differs between xml and database: $r19\n";

fclose($result_file);
$conn->close();

function data_comparison($database_result, $array_index, $sku, $product)
{
    global $result_file, $r7, $r8, $r9, $r10, $r11, $r12, $r13, $r14, $r15, $r16, $r17, $r18;
    if ($database_result != $data = isset($product[$array_index . 's'][$array_index]) ? normalize_array($product[$array_index . 's'][$array_index]) : []) {
        if (empty($database_result)) {
            fwrite($result_file, "SKU: " . $sku . "\n" . "There is $array_index data in the xml but not in the database.\n");
            if ($array_index == 'image') {
                $r7++;
            } else {
                $r13++;
            }
        } else if (empty($data)) {
            fwrite($result_file, "SKU: " . $sku . "\n" . "There is $array_index data in the database but not in the xml.\n");
            if ($array_index == 'image') {
                $r8++;
            } else {
                $r14++;
            }
        } else if (has_string_keys($data) && has_string_keys($database_result)) {
            foreach ($data as $xml_key => $xml_value) {
                if (key_exists($xml_key, $database_result)) {
                    if ($xml_value != $database_result[$xml_key]) {
                        fwrite($result_file, "SKU: $sku \n$array_index data: attribute $xml_key: " . print_r($xml_value, true) . " vs " . print_r($database_result[$xml_key], true) . "\n");
                        if ($array_index == 'image') {
                            $r9++;
                        } else {
                            $r15++;
                        }
                    }
                } else {
                    fwrite($result_file, "SKU: $sku \nImage data: Attribute $xml_key is not set in the database.\n");
                    if ($array_index == 'image') {
                        $r10++;
                    } else {
                        $r16++;
                    }
                }
            }
            foreach ($database_result as $database_key => $database_value) {
                if (!key_exists($database_key, $data)) {
                    fwrite($result_file, "SKU: $sku \n$array_index data: attribute $database_key is not set in the xml.\n");
                    if ($array_index == 'image') {
                        $r11++;
                    } else {
                        $r17++;
                    }
                }
            }
        } else if (!has_string_keys($data) && !has_string_keys($database_result) && count($data) == count($database_result)) {
            foreach ($data as $i => $xml_data) {
                foreach ($xml_data as $xml_key => $xml_value) {
                    if (key_exists($xml_key, $database_result[$i])) {
                        if ($xml_value != $database_result[$i][$xml_key]) {
                            fwrite($result_file, "SKU: $sku \n$array_index data: attribute $xml_key: " . print_r($xml_value, true) . " vs " . print_r($database_result[$i][$xml_key], true) . "\n");
                            if ($array_index == 'image') {
                                $r9++;
                            } else {
                                $r15++;
                            }
                        }
                    } else {
                        fwrite($result_file, "SKU: $sku \n$array_index data: attribute $xml_key is not set in the database.\n");
                        if ($array_index == 'image') {
                            $r10++;
                        } else {
                            $r16++;
                        }
                    }
                }
            }
            foreach ($database_result as $i => $database_data) {
                foreach ($database_data as $database_key => $database_value) {
                    if (!key_exists($database_key, $data[$i])) {
                        fwrite($result_file, "SKU: $sku \n$$array_index data: attribute $database_key is not set in the xml.\n");
                        if ($array_index == 'image') {
                            $r11++;
                        } else {
                            $r17++;
                        }
                    }
                }
            }
        }
        fwrite($result_file, "SKU: " . $sku . "\n" . "The $array_index data is:\n" . print_r($data, true) . "\nvs\n" . print_r($database_result, true) . "\n");
        if ($array_index == 'image') {
            $r12++;
        } else {
            $r18++;
        }
    }
}

function xml_to_array($dir)
{
    $path = __DIR__ . "/product_data.json";
    if (!file_exists($path)) {

        global $products;
        foreach (scandir($dir, $sorting_order = SCANDIR_SORT_DESCENDING) as $file) {
            if (pathinfo($dir . $file, PATHINFO_EXTENSION) == 'xml' && substr($file, 0, 6) == 'items_') {
                $file_name = basename($dir . $file);
                $sku_pos = strpos($file_name, "sku");
                $sku = substr($file_name, $sku_pos + 4, strlen($file_name) - ($sku_pos + 4) - 4);

                if (isset($products[$sku])) {
                    continue;
                }

                $xml = simplexml_load_file($dir . $file);
                foreach ($xml->product as $product) {
                    $product_data = [];
                    $json = json_encode($product);
                    $product = json_decode($json, true);
                    $sku = $product['sku'];
                    unset($product['sku']);
                    unset($product['attributes_list']);
                    unset($product['reset_website_ids']);

                    if (has_string_keys($product['attributes'])) {
                        unset($product['attributes']['CmWebText']);
                        unset($product['attributes']['CmSalesDescr']);
                        unset($product['attributes']['CmShortDescr']);
                    } else {
                        foreach ($product['attributes'] as $i => $attribute_group) {
                            unset($product['attributes'][$i]['CmWebText']);
                            unset($product['attributes'][$i]['CmSalesDescr']);
                            unset($product['attributes'][$i]['CmShortDescr']);
                        }
                    }

                    foreach ($product['attributes'] as $key => $value) {
                        if (empty($value)) {
                            $product['attributes'][$key] = '';
                        }
                    }
                    $products[$sku] = $product;
                }
            }
            if (count($products) > 250000) {
                break;
            }
        }
        file_put_contents($path, json_encode($products));
    } else {
        $data = file_get_contents($path);
        $products = json_decode($data, true);
    }
    return $products;
}

function normalize_array($array)
{
    return is_array($array) ? $array : [$array];
}

function has_string_keys($array)
{
    return count(array_filter(array_keys($array), 'is_string')) > 0;
}

function category_query()
{
    global $conn, $sku_string;
    $categories = [];

    $sql = "	SELECT catalog_product_entity.sku, catalog_category_entity_datetime.value AS 'category'
    FROM (((((catalog_product_entity
    JOIN sequence_product ON sequence_product.sequence_value = catalog_product_entity.entity_id)
    JOIN catalog_category_product ON catalog_category_product.product_id = sequence_product.sequence_value)
    JOIN sequence_catalog_category ON sequence_catalog_category.sequence_value = catalog_category_product.category_id)
    JOIN catalog_category_entity ON catalog_category_entity.entity_id = sequence_catalog_category.sequence_value)
    JOIN catalog_category_entity_datetime ON catalog_category_entity_datetime.row_id = catalog_category_entity.row_id)
   	WHERE catalog_product_entity.sku IN $sku_string

    UNION

    SELECT catalog_product_entity.sku, catalog_category_entity_decimal.value AS 'category'
    FROM (((((catalog_product_entity
    JOIN sequence_product ON sequence_product.sequence_value = catalog_product_entity.entity_id)
    JOIN catalog_category_product ON catalog_category_product.product_id = sequence_product.sequence_value)
    JOIN sequence_catalog_category ON sequence_catalog_category.sequence_value = catalog_category_product.category_id)
    JOIN catalog_category_entity ON catalog_category_entity.entity_id = sequence_catalog_category.sequence_value)
    JOIN catalog_category_entity_decimal ON catalog_category_entity_decimal.row_id = catalog_category_entity.row_id)
   	WHERE catalog_product_entity.sku IN $sku_string

    UNION

    SELECT catalog_product_entity.sku, catalog_category_entity_int.value AS 'category'
    FROM (((((catalog_product_entity
    JOIN sequence_product ON sequence_product.sequence_value = catalog_product_entity.entity_id)
    JOIN catalog_category_product ON catalog_category_product.product_id = sequence_product.sequence_value)
    JOIN sequence_catalog_category ON sequence_catalog_category.sequence_value = catalog_category_product.category_id)
    JOIN catalog_category_entity ON catalog_category_entity.entity_id = sequence_catalog_category.sequence_value)
    JOIN catalog_category_entity_int ON catalog_category_entity_int.row_id = catalog_category_entity.row_id)
   	WHERE catalog_product_entity.sku IN $sku_string

    UNION

    SELECT catalog_product_entity.sku, catalog_category_entity_text.value AS 'category'
    FROM (((((catalog_product_entity
    JOIN sequence_product ON sequence_product.sequence_value = catalog_product_entity.entity_id)
    JOIN catalog_category_product ON catalog_category_product.product_id = sequence_product.sequence_value)
    JOIN sequence_catalog_category ON sequence_catalog_category.sequence_value = catalog_category_product.category_id)
    JOIN catalog_category_entity ON catalog_category_entity.entity_id = sequence_catalog_category.sequence_value)
    JOIN catalog_category_entity_text ON catalog_category_entity_text.row_id = catalog_category_entity.row_id)
   	WHERE catalog_product_entity.sku IN $sku_string

    UNION

    (SELECT catalog_product_entity.sku, catalog_category_entity_varchar.value AS 'category'
    FROM (((((catalog_product_entity
    JOIN sequence_product ON sequence_product.sequence_value = catalog_product_entity.entity_id)
    JOIN catalog_category_product ON catalog_category_product.product_id = sequence_product.sequence_value)
    JOIN sequence_catalog_category ON sequence_catalog_category.sequence_value = catalog_category_product.category_id)
    JOIN catalog_category_entity ON catalog_category_entity.entity_id = sequence_catalog_category.sequence_value)
    JOIN catalog_category_entity_varchar ON catalog_category_entity_varchar.row_id = catalog_category_entity.row_id)
   	WHERE catalog_product_entity.sku IN $sku_string)


    ORDER BY sku";

    $result = $conn->query($sql);

    while ($row = $result->fetch_assoc()) {
        $categories[$row['sku']][] = $row['category'];
    }

    return $categories;
}

function data_query($attribute)
{
    global $conn, $sku_string;
    $attribute_data = [];

    $sql = "SELECT catalog_product_entity.sku, catalog_product_entity_text.value AS $attribute
            FROM ((catalog_product_entity_text
            JOIN catalog_product_entity ON catalog_product_entity.row_id = catalog_product_entity_text.row_id)
            JOIN eav_attribute ON eav_attribute.attribute_id = catalog_product_entity_text.attribute_id)
            WHERE eav_attribute.attribute_code = '$attribute' AND catalog_product_entity.sku IN $sku_string";

    $result = $conn->query($sql);
    while ($row = $result->fetch_assoc()) {
        $attribute_data[$row['sku']] = json_decode($row[$attribute], true);
    }
    foreach ($attribute_data as $sku => $data) {
        if ($data != null) {
            if (!has_string_keys($data) && count($data) == 1) {
                $attribute_data[$sku] = $data[0];
            }
        }
    }
    return $attribute_data;
}

function link_query()
{
    global $conn, $sku_string;
    $links = [];

    $sql = "SELECT parent_product.sku AS parent_sku, child_product.sku AS child_sku, catalog_product_link_type.code AS link_type
    FROM catalog_product_link
    INNER JOIN catalog_product_entity AS parent_product ON catalog_product_link.product_id = parent_product.row_id
    INNER JOIN catalog_product_entity AS child_product ON catalog_product_link.linked_product_id = child_product.entity_id
    INNER JOIN catalog_product_link_type ON catalog_product_link.link_type_id = catalog_product_link_type.link_type_id
    WHERE parent_product.type_id = 'simple' AND child_product.type_id = 'simple' AND parent_product.sku IN $sku_string";

    $result = $conn->query($sql);

    while ($row = $result->fetch_assoc()) {
        $links[$row['parent_sku']][] = $row['child_sku'];
    }

    return $links;
}

function base_query()
{
    global $conn, $sku_string;
    $attributes = [];

    $sql = "SELECT catalog_product_entity.sku, eav_attribute.attribute_code, catalog_product_entity_datetime.value, store.code AS 'store'
    FROM ((((((eav_attribute
    JOIN catalog_product_entity_datetime ON eav_attribute.attribute_id = catalog_product_entity_datetime.attribute_id)
    JOIN catalog_product_entity ON catalog_product_entity.row_id = catalog_product_entity_datetime.row_id)
    JOIN sequence_product ON sequence_product.sequence_value = catalog_product_entity.entity_id)
    JOIN catalog_product_website ON catalog_product_website.product_id = sequence_product.sequence_value)
    JOIN store_website ON store_website.website_id = catalog_product_website.website_id)
    JOIN store ON store.website_id = catalog_product_website.website_id)
    WHERE catalog_product_entity.sku IN $sku_string

    UNION

    SELECT catalog_product_entity.sku, eav_attribute.attribute_code, catalog_product_entity_decimal.value, store.code AS 'store'
    FROM ((((((eav_attribute
    JOIN catalog_product_entity_decimal ON eav_attribute.attribute_id = catalog_product_entity_decimal.attribute_id)
    JOIN catalog_product_entity ON catalog_product_entity.row_id = catalog_product_entity_decimal.row_id)
    JOIN sequence_product ON sequence_product.sequence_value = catalog_product_entity.entity_id)
    JOIN catalog_product_website ON catalog_product_website.product_id = sequence_product.sequence_value)
    JOIN store_website ON store_website.website_id = catalog_product_website.website_id)
    JOIN store ON store.website_id = catalog_product_website.website_id)
    WHERE catalog_product_entity.sku IN $sku_string

    UNION

    SELECT catalog_product_entity.sku, eav_attribute.attribute_code, catalog_product_entity_int.value, store.code AS 'store'
    FROM ((((((eav_attribute
    JOIN catalog_product_entity_int ON eav_attribute.attribute_id = catalog_product_entity_int.attribute_id)
    JOIN catalog_product_entity ON catalog_product_entity.row_id = catalog_product_entity_int.row_id)
    JOIN sequence_product ON sequence_product.sequence_value = catalog_product_entity.entity_id)
    JOIN catalog_product_website ON catalog_product_website.product_id = sequence_product.sequence_value)
    JOIN store_website ON store_website.website_id = catalog_product_website.website_id)
    JOIN store ON store.website_id = catalog_product_website.website_id)
    WHERE catalog_product_entity.sku IN $sku_string

    UNION

    SELECT catalog_product_entity.sku, eav_attribute.attribute_code, catalog_product_entity_text.value, store.code AS 'store'
    FROM ((((((eav_attribute
    JOIN catalog_product_entity_text ON eav_attribute.attribute_id = catalog_product_entity_text.attribute_id)
    JOIN catalog_product_entity ON catalog_product_entity.row_id = catalog_product_entity_text.row_id)
   	JOIN sequence_product ON sequence_product.sequence_value = catalog_product_entity.entity_id)
    JOIN catalog_product_website ON catalog_product_website.product_id = sequence_product.sequence_value)
    JOIN store_website ON store_website.website_id = catalog_product_website.website_id)
    JOIN store ON store.website_id = catalog_product_website.website_id)
    WHERE catalog_product_entity.sku IN $sku_string

    UNION

    (SELECT catalog_product_entity.sku, eav_attribute.attribute_code, catalog_product_entity_varchar.value, store.code AS 'store'
    FROM ((((((eav_attribute
    JOIN catalog_product_entity_varchar ON eav_attribute.attribute_id = catalog_product_entity_varchar.attribute_id)
    JOIN catalog_product_entity ON catalog_product_entity.row_id = catalog_product_entity_varchar.row_id)
    JOIN sequence_product ON sequence_product.sequence_value = catalog_product_entity.entity_id)
    JOIN catalog_product_website ON catalog_product_website.product_id = sequence_product.sequence_value)
    JOIN store_website ON store_website.website_id = catalog_product_website.website_id)
    JOIN store ON store.website_id = catalog_product_website.website_id)
    WHERE catalog_product_entity.sku IN $sku_string)

    ORDER BY sku, store";

    $result = $conn->query($sql);

    $i = 0;
    $sku = $store = null;
    while ($row = $result->fetch_assoc()) {
        if ($sku !== $row['sku']) {
            $i = 0;
            $attributes[$row['sku']][$i]['store'] = $row['store'];
        } else if ($store !== $row['store']) {
            $attributes[$row['sku']][++$i]['store'] = $row['store'];
        }
        $sku = $row['sku'];
        $attributes[$sku][$i][$row['attribute_code']] = $row['value'];
        $store = $row['store'];
    }

    return $attributes;
}
